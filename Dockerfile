FROM openjdk:17-alpine3.13

ARG TZ="Atlantic/Canary"
ARG JMETER_VERSION="5.4.1"
ARG JDBC_VERSION="42.2.20"

ENV JMETER_HOME="/opt/apache-jmeter" \
	JMETER_DOWNLOAD_URL="https://ftp.cixug.es/apache/jmeter/binaries/apache-jmeter-${JMETER_VERSION}.tgz" \
	TZ="${TZ}" \
	JDBC_DOWNLOAD_URL="https://jdbc.postgresql.org/download/postgresql-${JDBC_VERSION}.jar"

RUN apk add --no-cache --virtual .build-deps \
			curl=~7.77 \
            unzip=~6 \
	&& apk add --no-cache \
			tzdata=~2021a-r0 \
            bash=~5.1 \
            bind-tools \
	&& curl -o "/opt/apache-jmeter.tgz" -L ${JMETER_DOWNLOAD_URL} \
	&& tar -xvzf /opt/apache-jmeter.tgz -C /opt \
	&& mv "/opt/apache-jmeter-${JMETER_VERSION}" "${JMETER_HOME}" \
	&& rm -rf "${JMETER_HOME}/docs" \
	&& rm -rf /opt/apache-jmeter.tgz \
	&& curl -o "${JMETER_HOME}/lib/ext/postgresql-${JDBC_VERSION}.jar" -L ${JDBC_DOWNLOAD_URL} \
	&& apk del .build-deps

COPY rootfs /

WORKDIR	${JMETER_HOME}

ENTRYPOINT ["/usr/bin/entrypoint.sh"]