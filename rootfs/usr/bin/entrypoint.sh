#!/bin/bash
# Inspired from https://github.com/hhcordero/docker-jmeter-client
# Basically runs jmeter, assuming the PATH is set to point to JMeter bin-dir (see Dockerfile)
#
# This script expects the standdard JMeter command parameters.
#

# Install jmeter plugins available on /plugins volume
NODE_SERVICE_NAME="${NODE_SERVICE_NAME:-tasks.jmeter-node}"
JMETER_DEFAULT_ARGS="${JMETER_DEFAULT_ARGS:---nongui}"

if [ -d /plugins ]
then
    for plugin in /plugins/*.jar; do
        cp $plugin $(pwd)/lib/ext
    done;
fi

# Execute JMeter command
set -e
freeMem=`awk '/MemFree/ { print int($2/1024) }' /proc/meminfo`
s=$(($freeMem/10*8))
x=$(($freeMem/10*8))
n=$(($freeMem/10*2))
export JVM_ARGS="-Xmn${n}m -Xms${s}m -Xmx${x}m"

if [ "${ROLE}" == "client" ]
then
    nodeIps=$(dig "${NODE_SERVICE_NAME}" +short | xargs | sed -e 's/ /,/g')
    echo "Nodes of service ${NODE_SERVICE_NAME}:"
    echo "${nodeIps}"
    JMETER_DEFAULT_ARGS="${JMETER_DEFAULT_ARGS} -R ${nodeIps}"
fi

echo "START Running Jmeter on `date`"
echo "JVM_ARGS=${JVM_ARGS}"
echo "jmeter ${JMETER_DEFAULT_ARGS} $@"

# Keep entrypoint simple: we must pass the standard JMeter arguments
./bin/jmeter ${JMETER_DEFAULT_ARGS} $@
echo "END Running Jmeter on `date`"
